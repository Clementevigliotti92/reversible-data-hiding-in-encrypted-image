A simple Matlab implementation of the algorithm presented in the paper: "Reversible-data-hiding-in-Encrypted-image" (Xinpeng Zhang)

link: http://pgembeddedsystems.com/download/matlab/Reversible%20Data%20Hiding%20in%20Encrypted%20Image.pdf

[An in-depth description (in Italian) is present in the pdf included in the repository.]

In this implementation two Stream Ciphers were used: -Trivium -RC4

Implemented features:
*   Image upload and grayscale conversion (two-dimensional image with 8-bit depth);
*   Conversion of data to hide in bit string;
*   Calculation of the possible lengths of the bit string to be inserted;
*   Encryption (and decryption) of the image with RC4 stream cipher;
*   Parallelization of streamciphers (RC4 and Trivium) [Requirements: 'parallel computing toolbox'];
*   Encryption (and decryption) of the image with Trivium (not recommended);
*   Parallelization of Trivium (not recommended);
*   Entering data to hide: to. extraction of the block (i, j) of size s; b. flip of the block based on the bit to be inserted and based on the pseudo-random division defined by the stream cipher (RC4 or Trivium);
*   Extraction of data and original image;
*   Conversion of the string of bits inserted into character string.

Sender flow:

![](sender.JPG)

Receiver flow:

![](recevier.JPG)